extends CanvasLayer


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var destination

func change_scene(dest:NodePath):
	destination = dest
	$AnimationPlayer.play("trans")

func go_to_scene():
	get_tree().change_scene(destination)
