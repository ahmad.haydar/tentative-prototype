extends Area2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var unlocked = false
export (Array, NodePath) var toggles = []
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _process(delta):
	unlocked = true
	for i in toggles:
		if get_node(i).status == "no":
			unlocked = false
			break
	if unlocked:
		$Sprite.frame = 1
	else:
		$Sprite.frame = 0


func _on_Area2D_body_entered(body):
	if body.is_in_group("player") and unlocked:
		SceneManager.change_scene("res://Scenes/level_01.tscn")
